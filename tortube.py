#!/usr/bin/env python2.7
# tortube.py -- youtube-dl for identity issuses 
# Copyright (C) 2016  Jordan Russell <jordan.likes.curry@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals
import youtube_dl
import argparse # :>
import getpass
import stem
import stem.control
import sys
import time


mydl = youtube_dl.YoutubeDL

class Tortube(mydl):
    def __init__(self, options, tor_options):
        self.socks5_port = tor_options.get("socks5_port", 9150)
        self.control_port = tor_options.get("tor_control_port", 9151)
        self.nodes = map(lambda s: '{'+s+'}', tor_options.get("nodes", []))
        self.use_original = self.nodes == []
        self.wait_time = 10
        self.tor_conn = None

        options['proxy'] = "socks5://127.0.0.1:{}".format(self.socks5_port)

        mydl.__init__(self, options)

        self.setup_tor_conn(self.control_port)

        self.original_exit_nodes = self.tor_conn.get_conf("ExitNodes")

    def setup_tor_conn(self, port):
        try:
            tor_conn = stem.control.Controller.from_port(port = port)
            self.to_screen("connected")
        except stem.SocketError as exc:
            self.to_stderr("[Error] Failed to connect to tor on port {}: {}".format(port, exc))
            sys.exit(1)

        try:
            tor_conn.authenticate()
        except stem.connection.MissingPassword:
            self.get_password_auth(tor_conn)
               
        self.to_screen("authenticated")

        if isinstance(self.tor_conn, stem.control.Controller):
            self.tor_conn.close()
            
        self.tor_conn = tor_conn

    def get_password_auth(self, tor_conn):
        pswd = getpass.getpass("Tor control password: ")
        success = False
        for i in xrange(3):
            try:
                tor_conn.authenticate(password = pswd)
                success = True
                break
            except stem.connection.AuthenticationFailure:
                pswd = getpass.getpass("Tries[{}/3]Tor control password: ".format(i+1))
                success = False
        if not success:
            self.to_stderr("The programmer is not sure how to use passwords correctly")
            self.to_stderr("I'm sorry ;_;")
            sys.exit(1)

    def restore_exitnodes(self):
        self.tor_conn.set_conf("ExitNodes", self.original_exit_nodes)

    def __enter__(self):
        mydl.__enter__(self)
        return self

    def __exit__(self, *args):
        mydl.__exit__(self, *args)
        self.restore_exitnodes()
        self.tor_conn.close()

    def download(self, urls):
        # mydl.download(self, urls)
        to_download = urls[:]
        while to_download != []:
            for url in to_download:
                try:
                    mydl.download(self, [url])
                    to_download.remove(url)
                except youtube_dl.utils.DownloadError:
                    pass
                except Exception, e:
                    self.to_stderr("Failed to download {} skipping...: {}".format(url, e))
                    to_download.remove(url)
            if to_download != []:
                self.to_screen("Some videos failed to download: {}".format(to_download))
                self.to_screen("Waiting {} seconds and then trying again with a new identity".format(self.wait_time))
                time.sleep(self.wait_time)
                self.get_new_identity()

    # if no nodes were set originally we will just use the already set exitnodes
    def get_next_node(self):
        if self.nodes == []:
            return False
        else:
            return self.nodes.pop(0)
        
    def newnym(self):
        # waittime = self.tor_conn.get_newnym_wait()
        # time.sleep(waittime)
        if not self.tor_conn.is_newnym_available():
            waittime = self.tor_conn.get_newnym_wait()
            self.to_stderr("tor is making us wait for {} before getting newnym".format(waittime))
            time.sleep(waittime)
        self.tor_conn.signal("NEWNYM")
        self.to_screen("Got new identity")
        
    def get_new_identity(self):
        def setup_next_node():
            next_node = self.get_next_node()
            if next_node:
                try:
                    self.tor_conn.set_conf("ExitNodes", [next_node])
                    self.to_screen("Set exit node {}".format(next_node))
                except (stem.InvalidArguments, stem.InvalidRequest) as exc:
                    self.to_stderr("[WARNING] invalid node name {}, {}".format(next_node,exc))
                    setup_next_node()
            else:
                self.tor_conn.set_conf("ExitNodes", "") # using nothing for random
                self.use_original = True

        if not self.use_original:
            setup_next_node()

        self.newnym()

# yes!
def cons(a,b):
    res = [a]
    res.extend(b)
    return res

def parse_tor_opts():
    aparser = argparse.ArgumentParser(description='hacks!', add_help=False)
    group = aparser.add_argument_group("Tor options")
    group.add_argument("--nodes", type=str, action="store",
                       default="",
                       help='The nodes to try first in order\nEX: --nodes "jp fr us"')
    group.add_argument("--socks5-port", type=int, action="store",
                       default=9150,
                       help="The port that tor is listening on")
    group.add_argument("--tor-control-port", type=int, action="store",
                       default=9151,
                       help="The port that tor control is listening on.")
    aparser.add_argument("-h", "--help", action="store_true")

    ns, next_args = aparser.parse_known_args(sys.argv[1:])

    ns = ns.__dict__

    if len(ns["nodes"]) > 0:
        ns["nodes"] = ns["nodes"].split(" ")
    else:
        ns["nodes"] = []

    if ns["help"]:
        print("tortube specific options: {}".format(aparser.format_help()))
        youtube_dl.options.parseOpts(["--help"])
        # probably not reached
        sys.exit(1)

    sys.argv = cons("", next_args)
    return ns

def parse_ydl_opts():
    _, opts, args  = youtube_dl.options.parseOpts()

    opts = opts.__dict__

    return opts, args

def main():
    tor_opts = parse_tor_opts()
    # these hacks are wack yo
    youtube_dl.YoutubeDL = lambda ydl: Tortube(ydl, tor_opts)
    try:
        youtube_dl._real_main(sys.argv[1:])
    except KeyboardInterrupt:
        print("Caught Ctrl-C... Exiting")
        sys.exit(1)

if __name__ == '__main__':
    main()
