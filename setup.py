#!/usr/bin/env python2.7
from setuptools import setup 

setup(name="tortube",
      version="1.0",
      description="youtube-dl for identity issuses",
      author="Jordan Russell",
      author_email="jordan.likes.curry@gmail.com",
      url="https://gitlab.com/jordanr/tortube",
      py_modules=['tortube'],
      entry_points = {
          'console_scripts': ['tortube = tortube:main']
          }
      )
