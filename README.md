# Tortube.py
Use this script (sparingly) to download geo-blocked youtube videos.

I got tired of clicking new identity over and over again on tor browser so I made this script.

Please do not use this script too much or as a replacement for just regular youtube-dl.
Abusing the tor network is not nice.

## Installation
`python2.7 setup.py install`

This package require `stem` and `youtube-dl`. Then make sure you either have tor running or you are running the tor-browser-bundle. 


## Usage
`tortube` is a wrapper around youtube-dl. So naturally all the usual youtube-dl options should work.

The following are the tortube specific options for specifying proxy, ports, and nodes.


```` 
Usage: tortube.py [--nodes NODES] [--socks5-port SOCKS5_PORT]
                  [--tor-control-port TOR_CONTROL_PORT] [-h]

optional arguments:
  -h, --help

Tor options:
  --nodes NODES         The nodes to try first in order EX: --nodes "jp fr us"
  --socks5-port SOCKS5_PORT
                        The port that tor is listening on
  --tor-control-port TOR_CONTROL_PORT
                        The port that tor control is listening on.
                        
                        
````
